#include "ofApp.h"

#define WIDTH 30
#define HEIGHT 5

//--------------------------------------------------------------
void ofApp::setup(){
  ofBackground(0);
  ofSetFrameRate(60);
  
  numXTiles = 30;
  numYTiles = 30;
  
  spacingX = ofGetWidth() / numXTiles;
  spacingY = ofGetHeight() / numYTiles;
  
  startX = spacingX / 2;
  startY = spacingY / 2;
  
  ofSetRectMode(OF_RECTMODE_CENTER);
  
  ofSetCircleResolution(50);
  
  color1 = ofColor(131,63,62);
  color2 = ofColor(233,113,42);
  color3 = ofColor(23,66,142);
  color4 = ofColor(164,95,56);
  
  bMouseCurrent = false;
  
}




//--------------------------------------------------------------
void ofApp::update(){
  
}

//--------------------------------------------------------------
void ofApp::draw(){
  
  if (bMouseCurrent) {
    drawGrid(true, 0.1, 3.0, color3, color4);
    drawGrid(false, 3, 0.25, color1, color2);
  }
  
}

void ofApp::drawGrid(bool bAttract, float fMinSize, float fMaxSize, ofColor color1, ofColor color2) {
  for (int y=0;y<numYTiles;y++) {
    for (int x=0;x<numXTiles;x++) {
      int locX = (x + 0.5) * spacingX;
      int locY = (y + 0.5) * spacingY;
      ofPushMatrix();
      ofTranslate(locX,locY);
      
      float distanceFromMouse = ofDist(mouseX,mouseY,locX,locY);
      float angle = atan2(mouseY - locY, mouseX - locX);
      if (!bAttract) {
        angle += 180;
      }
      ofRotateDeg(ofRadToDeg(angle));
      ofTranslate(ofMap(distanceFromMouse,0,ofGetWidth(),80,100,true),0);
      ofScale(ofMap(distanceFromMouse,0,ofGetWidth(),fMinSize,fMaxSize,true));
      ofSetColor(color1.getLerped(color2,ofMap(distanceFromMouse,0,ofGetWidth(),0,1,true)));
      ofDrawCircle(0,0,10);
      ofPopMatrix();
    }
  }
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
  
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
  bMouseCurrent = true;
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
  
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
  
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
  
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
  
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
  
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
  
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
  
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
  
}
